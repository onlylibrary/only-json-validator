#only-json-validator

--- 这是一个用来校验json字符串的小框架

###使用方式（主要有3部分）
1、配置文件，用于配置验证规则
2、Java验证代码读取配置文件
3、需要校验的json字符串
```javascript

/**
 * --配置方式,内容配置在json文件中<br>
 * 
 */
 [
	{
		"name": "001",//这个相当于命名空间或者id等,不然在web项目中可以用方法路径如 /user/add
		"validator": {
			"array": {//这个是需要验证的字段 可以是任意命名的，这里demo方便理解用了相应需要验证类型命名
				"validators": [//这是验证规则 可以配置多个，除了非空验证，当这个字段无值或者空，是跳过验证的
					{
						"validator": "array"//只是表示这个字段为数组 其他的验证类型如email，boolean
					}
				]
			},
			"boolean": {
				"validators": [
					{
						"validator": "boolean"
					}
				]
			},
			"email": {
				"validators": [
					{
						"validator": "email"
					}
				],
				"nodes": {
					"": {
						
					},
					"nodes": {
						
					}
				}
			},
			"equalsTo": {
				"validators": [
					{
						"validator": "equalsTo",
						"value": "node.equalsTo"
					}
				]
			},
			"inStringArray": {
				"validators": [
					{
						"validator": "inStringArray",
						"value": [
							"1",
							"2",
							"3",
							"a",
							"b",
							"c"
						]
					}
				]
			},
			"integer": {
				"validators": [
					{
						"validator": "integer"
					}
				]
			},
			"maxLength": {
				"validators": [
					{
						"validator": "maxLength",
						"value": 10
					}
				]
			},
			"max": {
				"validators": [
					{
						"validator": "max",
						"value": "50.5"
					}
				]
			},
			"minLength": {
				"validators": [
					{
						"validator": "minLength",
						"value": 5
					}
				]
			},
			"min": {
				"validators": [
					{
						"validator": "min",
						"value": "20.0"
					}
				]
			},
			"notBlank": {
				"validators": [
					{
						"validator": "notBlank"
					}
				]
			},
			"notNull": {
				"validators": [
					{
						"validator": "notNull"
					}
				]
			},
			"notEmpty": {
				"validators": [
					{
						"validator": "notNull"
					}
				]
			},
			"number": {
				"validators": [
					{
						"validator": "number"
					}
				]
			},
			"regex": {
				"validators": [
					{
						"validator": "regex",
						"value": "((\\d{2}(([02468][048])|([13579][26]))[\\-]((((0?[13578])|(1[02]))[\\-]((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-]((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-]((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-]((((0?[13578])|(1[02]))[\\-]((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-]((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-]((0?[1-9])|(1[0-9])|(2[0-8]))))))",
						"message": "日期格式不对"
					}
				]
			},
			"url": {
				"validators": [
					{
						"validator": "url"
					}
				]
			},
			"code": {
				"validators": [
					{
						"validator": "notBlank",
						"code": "code.001",
						"message": "不能为空！"
					}
				]
			},
			"roleIds": {
				"validators": [
					{
						"validator": "array"
					}
				],
				"nodes": {
					"": {
						"validators": [
							{
								"validator": "notNull"
							},
							{
								"validator": "integer"
							}
						]
					}
				}
			},
			"nodes": {
				"validators": [
					{
						"validator": "array"
					},
					{
						"validator": "notNull"
					}
				],
				"nodes": {
					"": {
						"validators": [
							{
								"validator": "array"
							},
							{
								"validator": "notNull"
							},
							{
								"validator": "integer"
							}
						],
						"nodes": {
							"": {
								"validators": [
									{
										"validator": "array"
									},
									{
										"validator": "notNull"
									},
									{
										"validator": "integer"
									}
								],
								"nodes": {
									"name": {
										"validators": [
											{
												"validator": "notNull"
											},
											{
												"validator": "integer"
											}
										]
									}
								}
							}
						}
					}
				}
			},
			"node": {
				"validators": [
					{
						"validator": "notNull"
					}
				],
				"nodes": {
					"array": {
						"validators": [
							{
								"validator": "array"
							}
						]
					},
					"boolean": {
						"validators": [
							{
								"validator": "boolean"
							}
						]
					},
					"email": {
						"validators": [
							{
								"validator": "email"
							}
						]
					},
					"equalsTo": {
						"validators": [
							{
								"validator": "equalsTo",
								"value": "equalsTo"
							}
						]
					},
					"inStringArray": {
						"validators": [
							{
								"validator": "inStringArray",
								"value": [
									"1",
									"2",
									"3",
									"a",
									"b",
									"c"
								]
							}
						]
					},
					"integer": {
						"validators": [
							{
								"validator": "integer"
							}
						]
					},
					"maxLength": {
						"validators": [
							{
								"validator": "maxLength",
								"value": 10
							}
						]
					},
					"max": {
						"validators": [
							{
								"validator": "max",
								"value": "50.5"
							}
						]
					},
					"minLength": {
						"validators": [
							{
								"validator": "minLength",
								"value": 5
							}
						]
					},
					"min": {
						"validators": [
							{
								"validator": "min",
								"value": "20.0"
							}
						]
					},
					"notBlank": {
						"validators": [
							{
								"validator": "notBlank"
							}
						]
					},
					"notNull": {
						"validators": [
							{
								"validator": "notNull"
							}
						]
					},
					"number": {
						"validators": [
							{
								"validator": "number"
							}
						]
					},
					"regex": {
						"validators": [
							{
								"validator": "regex",
								"value": "((\\d{2}(([02468][048])|([13579][26]))[\\-]((((0?[13578])|(1[02]))[\\-]((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-]((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-]((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-]((((0?[13578])|(1[02]))[\\-]((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-]((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-]((0?[1-9])|(1[0-9])|(2[0-8]))))))",
								"message": "日期格式不对"
							}
						]
					},
					"url": {
						"validators": [
							{
								"validator": "url"
							}
						]
					},
					"code": {
						"validators": [
							{
								"validator": "notBlank",
								"code": "code.001",
								"message": "不能为空！"
							}
						]
					},
					"roleIds": {
						"validators": [
							{
								"validator": "array"
							}
						],
						"nodes": {
							"": {
								"validators": [
									{
										"validator": "notNull"
									},
									{
										"validator": "integer"
									}
								]
							}
						}
					}
				}
			}
		}
	},
	{
		"name": "002",
		"validator": {
			"name": {
				"validators": [
					{
						"validator": "notBlank"
					}
				]
			},
			"age": {
				"validators": [
					{
						"validator": "max",
						"value": "100"
					},
					{
						"validator": "min",
						"value": "10"
					}
				]
			},
			"email": {
				"validators": [
					{
						"validator": "email"
					}
				]
			},
			"sex": {
				"validators": [
					{
						"validator": "notNull"
					},
					{
						"validator": "inStringArray",
						"value": [
							"男",
							"女",
							"保密"
						]
					}
				]
			},
			"head": {
				"validators": [
					{
						"validator": "url"
					}
				]
			},
			"index": {
				"validators": [
					{
						"validator": "number"
					}
				]
			},
			"roleIds": {
				"validators": [
					{
						"validator": "array"
					}
				],
				"nodes": {
					"": {
						"validators": [
							{
								"validator": "notNull"
							},
							{
								"validator": "integer"
							}
						]
					}
				}
			}
		}
	},
	{
		"name": "003",
		"validator": {
			"name": {
				"validators": [
					{
						"validator": "notBlank",
						"code": "10001",
						"message": "姓名不能为空"
					}
				]
			},
			"roles": {
				"validators": [
					{
						"validator": "array"
					}
				],
				"nodes": {
					"": {
						"validators": [
							{
								"validator": "array"
							}
						],
						"nodes": {
							"name": {
								"validators": [
									{
										"validator": "notBlank",
										"code": "10001",
										"message": "姓名不能为空"
									}
								]
							}
						}
					}
				}
			}
		}
	}
]
 


/**
 * --需要校验的json示例<br>
 * 
 */
 {
  "array": "array",
  "boolean": 1,
  "email": "email",
  "equalsTo": "node.equalsTo",
  "inStringArray": "inStringArray",
  "integer": "integer",
  "maxLength": "1234567891011",
  "max": "100",
  "minLength": "1",
  "min": "2",
  "notBlank": "",
  "notNull": null,
  "number": "number",
  "regex": "regex",
  "url": "url",
  "code": "",
  "node": {
    "array": "array",
    "boolean": 1,
    "email": "email",
    "equalsTo": "equalsTo",
    "inStringArray": "inStringArray",
    "integer": "integer",
    "maxLength": "1234567891011",
    "max": "100",
    "minLength": "1",
    "min": "2",
    "notBlank": "",
    "notNull": null,
    "number": "number",
    "regex": "regex",
    "url": "url",
    "code": "",
    "roleIds": [
      "",
      "g",
      "b"
    ]
  },
  "roleIds": {
  }
}
```


Java使用方式
```java
String json = "{\"name\":\"\"}";
ValidatorService vs = new ValidatorService("classpath*:/validator/*.json");
List<ValidatorResult> list = vs.validate(json, "001");
```