/**
 * validator-item.json<br>
 * 此文件中是支持的验证类型
 */
/**
 * config-demo.json<br>
 * 此文件中是配置验证的demo
 */
/**
 * data-demo.json<br>
 * 此文件中是需要验证的json数据demo
 */

/**
 * 使用方式：<br>
 * String json = "{"name":""}";<br>
 * ValidatorService vs = new ValidatorService("classpath*:/validator/*.json");<br>
 * List<ValidatorResult> list = vs.validate(json, "001");<br>
 */
