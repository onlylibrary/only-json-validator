package com.onlyxiahui.framework.json.validator;

/**
 * 
 * date 2018-12-18 09:44:40<br>
 * description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class ValidatorResult {

	private String propertyPath;
	private String message;
	private String errorCode;
	private Object extend;

	public ValidatorResult() {

	}

	public ValidatorResult(String propertyPath, String errorCode, String message) {
		this.propertyPath = propertyPath;
		this.errorCode = errorCode;
		this.message = message;
	}

	public String getPropertyPath() {
		return propertyPath;
	}

	public void setPropertyPath(String propertyPath) {
		this.propertyPath = propertyPath;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Object getExtend() {
		return extend;
	}

	public void setExtend(Object extend) {
		this.extend = extend;
	}
}
