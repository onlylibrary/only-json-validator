package com.onlyxiahui.framework.json.validator.impl;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.json.validator.Validator;
import com.onlyxiahui.framework.json.validator.ValidatorContext;
import com.onlyxiahui.framework.json.validator.ValidatorData;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.error.ErrorInfoEnum;

/**
 * 
 * Date 2018-12-26 15:16:58<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class JsonBooleanValidator implements Validator<JSONObject, Object> {

	@Override
	public String getKey() {
		return "boolean";
	}

	@Override
	public boolean check(Object judge) {
		return true;
	}

	public String getMessage() {
		return "只能是“true”或者“false”！";
	}

	public String getCode() {
		return ErrorInfoEnum.TYPE_ERROR.code();
	}

	@Override
	public ValidatorResult valid(ValidatorContext validatorContext, ValidatorData<JSONObject, Object> vd) {
		// JSONObject data = vd.getData();
		Object attribute = vd.getAttribute();
		// Object judge = vd.getJudge();
		String code = vd.getCode();
		String message = vd.getMessage();

		ValidatorResult vr = null;
		boolean mark = true;
		if (null != attribute) {
			String text = attribute.toString();
			mark = "true".equals(text) || "false".equals(text);
		}
		if (!mark) {
			vr = new ValidatorResult();
			vr.setErrorCode(code == null ? this.getCode() : code);
			vr.setMessage(message == null ? this.getMessage() : message);
			vr.setExtend(vd.getExtend());
		}
		return vr;
	}
}
