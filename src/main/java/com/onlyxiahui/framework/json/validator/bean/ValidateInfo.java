package com.onlyxiahui.framework.json.validator.bean;

/**
 * Description <br>
 * Date 2020-06-11 10:59:37<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ValidateInfo {

	private String name;
	private String description;
	private ValidatorInfo validator = new ValidatorInfo();

	public ValidateInfo() {
	}

	public ValidateInfo(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ValidatorInfo getValidator() {
		return validator;
	}

	public void setValidator(ValidatorInfo validator) {
		this.validator = validator;
	}

	public void addProperty(String key, ValidatorProperty value) {
		validator.put(key, value);
	}

	public ValidatorProperty buildProperty(String key) {
		ValidatorProperty vp = new ValidatorProperty();
		this.addProperty(key, vp);
		return vp;
	}
}
