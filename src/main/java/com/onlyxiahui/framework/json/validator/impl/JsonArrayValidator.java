package com.onlyxiahui.framework.json.validator.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.json.validator.Validator;
import com.onlyxiahui.framework.json.validator.ValidatorContext;
import com.onlyxiahui.framework.json.validator.ValidatorData;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.error.ErrorInfoEnum;

/**
 * 
 * Date 2018-12-26 15:15:47<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class JsonArrayValidator implements Validator<JSONObject, Object> {

	String message = "只能是数组！";

	@Override
	public String getKey() {
		return "array";
	}

	@Override
	public boolean check(Object judge) {
		return true;
	}

	@Override
	public ValidatorResult valid(ValidatorContext validatorContext, ValidatorData<JSONObject, Object> vd) {
		// JSONObject data = vd.getData();
		Object attribute = vd.getAttribute();
		// Object judge = vd.getJudge();
		String code = vd.getCode();
		String message = vd.getMessage();

		ValidatorResult vr = null;
		boolean mark = true;
		if (null != attribute) {
			boolean isCollection = attribute instanceof Collection;
			boolean isList = attribute instanceof List;
			boolean isSet = attribute instanceof Set;
			boolean isArray = attribute.getClass().isArray();
			mark = isCollection || isList || isSet || isArray;
		}
		if (!mark) {
			vr = new ValidatorResult();
			vr.setErrorCode(code == null ? this.getCode() : code);
			vr.setMessage(message == null ? this.message : message);
			vr.setExtend(vd.getExtend());
		}
		return vr;
	}

	public String getCode() {
		return ErrorInfoEnum.TYPE_ERROR.code();
	}
}
