package com.onlyxiahui.framework.json.validator.impl;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.json.validator.Validator;
import com.onlyxiahui.framework.json.validator.ValidatorContext;
import com.onlyxiahui.framework.json.validator.ValidatorData;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.error.ErrorInfoEnum;
import com.onlyxiahui.framework.json.validator.util.RegexValidatorUtil;

/**
 * 
 * Date 2018-12-26 17:23:34<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class JsonMinLengthValidator implements Validator<JSONObject, Object> {

	@Override
	public String getKey() {
		return "minLength";
	}

	@Override
	public boolean check(Object judge) {
		return null != judge && RegexValidatorUtil.isInteger(judge.toString());
	}

	@Override
	public ValidatorResult valid(ValidatorContext validatorContext, ValidatorData<JSONObject, Object> vd) {
		// JSONObject data = vd.getData();
		Object attribute = vd.getAttribute();
		Object judge = vd.getJudge();
		String code = vd.getCode();
		String message = vd.getMessage();

		ValidatorResult vr = null;
		boolean mark = true;
		int minLength = 0;
		if (null != attribute && null != judge) {
			String valueText = attribute.toString();
			String judgeText = judge.toString();
			boolean isJudgeInteger = RegexValidatorUtil.isInteger(judgeText);
			if (isJudgeInteger) {
				minLength = Integer.parseInt(judgeText);
				mark = (valueText.length()) >= minLength;
			}
		}
		if (!mark) {
			vr = new ValidatorResult();
			vr.setErrorCode(code == null ? this.getCode() : code);
			vr.setMessage(message == null ? this.getMessage(minLength) : message);
			vr.setExtend(vd.getExtend());
		}
		return vr;
	}

	public String getMessage(int minLength) {
		StringBuilder message = new StringBuilder("长度不能短于");
		message.append(minLength);
		message.append("！");
		return message.toString();
	}

	public String getCode() {
		return ErrorInfoEnum.RANGE_ERROR.code();
	}
}
