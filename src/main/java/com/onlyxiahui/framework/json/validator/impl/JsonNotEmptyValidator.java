package com.onlyxiahui.framework.json.validator.impl;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.json.validator.Validator;
import com.onlyxiahui.framework.json.validator.ValidatorContext;
import com.onlyxiahui.framework.json.validator.ValidatorData;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.error.ErrorInfoEnum;

/**
 * 
 * Date 2018-12-26 15:49:52<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class JsonNotEmptyValidator implements Validator<JSONObject, Object> {

	@Override
	public String getKey() {
		return "notEmpty";
	}

	@Override
	public boolean check(Object judge) {
		return true;
	}

	@Override
	public ValidatorResult valid(ValidatorContext validatorContext, ValidatorData<JSONObject, Object> vd) {
		// JSONObject data = vd.getData();
		Object attribute = vd.getAttribute();
		// Object judge = vd.getJudge();
		String code = vd.getCode();
		String message = vd.getMessage();

		ValidatorResult vr = null;
		String text = (null == attribute) ? null : attribute.toString();
		boolean mark = isNotEmpty(text);

		if (!mark) {
			vr = new ValidatorResult();
			vr.setErrorCode(code == null ? this.getCode() : code);
			vr.setMessage(message == null ? this.getMessage() : message);
			vr.setExtend(vd.getExtend());
		}
		return vr;
	}

	public String getCode() {
		return ErrorInfoEnum.IS_EMPTY.code();
	}

	public String getMessage() {
		return "不能为空字符串！";
	}

	private boolean isNotEmpty(final String cs) {
		return null == cs || !cs.isEmpty();
	}
}
