package com.onlyxiahui.framework.json.validator;

/**
 * 
 * Date 2018-12-26 10:38:55<br>
 * Description 校验接口
 * 
 * @author XiaHui<br>
 * @param <D>
 * @param <A>
 * @since 1.0.0
 */
public interface Validator<D, A> {
	/**
	 * 
	 * Date 2018-12-26 15:34:52<br>
	 * Description 获取验证器的key
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public String getKey();

	/**
	 * 
	 * Description <br>
	 * Date 2019-11-01 11:14:02<br>
	 * 
	 * @param judge 判断条件
	 * @return boolean
	 * @since 1.0.0
	 */
	public boolean check(Object judge);

	/**
	 * 
	 * Description 进行验证 Description <br>
	 * Date 2019-11-19 20:05:06<br>
	 * 
	 * @param validatorContext 全局上下文
	 * @param vd               验证条件
	 * @return ValidatorResult
	 * @since 1.0.0
	 */
	public ValidatorResult valid(ValidatorContext validatorContext, ValidatorData<D, A> vd);

}
