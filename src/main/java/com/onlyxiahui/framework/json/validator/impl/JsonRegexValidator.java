package com.onlyxiahui.framework.json.validator.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.json.validator.Validator;
import com.onlyxiahui.framework.json.validator.ValidatorContext;
import com.onlyxiahui.framework.json.validator.ValidatorData;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.error.ErrorInfoEnum;

/**
 * 
 * Date 2018-12-26 15:29:38<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class JsonRegexValidator implements Validator<JSONObject, Object> {

	@Override
	public String getKey() {
		return "regex";
	}

	@Override
	public boolean check(Object judge) {
		return null != judge && !judge.toString().isEmpty();
	}

	@Override
	public ValidatorResult valid(ValidatorContext validatorContext, ValidatorData<JSONObject, Object> vd) {
		// JSONObject data = vd.getData();
		Object attribute = vd.getAttribute();
		Object judge = vd.getJudge();
		String code = vd.getCode();
		String message = vd.getMessage();

		ValidatorResult vr = null;
		boolean mark = true;
		if (null != attribute && null != judge) {
			String text = attribute.toString();
			String regex = judge.toString();
			if (!"".equals(regex)) {
				mark = match(regex, text);
			}
		}
		if (!mark) {
			vr = new ValidatorResult();
			vr.setErrorCode(code == null ? this.getCode() : code);
			vr.setMessage(message == null ? this.getMessage() : message);
			vr.setExtend(vd.getExtend());
		}
		return vr;
	}

	public String getMessage() {
		return "";
	}

	public String getCode() {
		return ErrorInfoEnum.MATCHING_ERROR.code();
	}

	private boolean match(String regex, String text) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		return matcher.matches();
	}
}
