package com.onlyxiahui.framework.json.validator.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Description <br>
 * Date 2020-06-11 11:07:13<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ValidatorProperty {

	private List<ValidatorRule> validators = new ArrayList<>();
	private ValidatorInfo nodes = new ValidatorInfo();

	public List<ValidatorRule> getValidators() {
		return validators;
	}

	public void setValidators(List<ValidatorRule> validators) {
		this.validators = validators;
	}

	public ValidatorInfo getNodes() {
		return nodes;
	}

	public void setNodes(ValidatorInfo nodes) {
		this.nodes = nodes;
	}

	public ValidatorProperty addRule(String validator) {
		return this.addRule(new ValidatorRule(validator));
	}

	public ValidatorProperty addRule(String validator, String message) {
		return this.addRule(new ValidatorRule(validator, message));
	}

	public ValidatorProperty addRule(String validator, String message, String code) {
		return this.addRule(new ValidatorRule(validator, message, code));
	}

	public ValidatorProperty addRule(String validator, String message, String code, Object value) {
		return this.addRule(new ValidatorRule(validator, message, code, value));
	}

	public ValidatorProperty addRule(String validator, String message, String code, Object value, Object extend) {
		return this.addRule(new ValidatorRule(validator, message, code, value, extend));
	}

	public ValidatorProperty addRule(ValidatorRule rule) {
		validators.add(rule);
		return this;
	}
}
