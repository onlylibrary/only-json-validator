package com.onlyxiahui.framework.json.validator.bean;

/**
 * Description <br>
 * Date 2020-06-11 10:53:19<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ValidatorRule {

	private String validator;
	private String message;
	private String code;
	private Object value;
	private Object extend;

	public ValidatorRule() {
		super();
	}

	public ValidatorRule(String validator) {
		super();
		this.validator = validator;
	}

	public ValidatorRule(String validator, String message) {
		super();
		this.validator = validator;
		this.message = message;
	}

	public ValidatorRule(String validator, String message, String code) {
		super();
		this.validator = validator;
		this.message = message;
		this.code = code;
	}

	public ValidatorRule(String validator, String message, String code, Object value) {
		super();
		this.validator = validator;
		this.message = message;
		this.code = code;
		this.value = value;
	}

	public ValidatorRule(String validator, String message, String code, Object value, Object extend) {
		super();
		this.validator = validator;
		this.message = message;
		this.code = code;
		this.value = value;
		this.extend = extend;
	}

	public String getValidator() {
		return validator;
	}

	public void setValidator(String validator) {
		this.validator = validator;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getExtend() {
		return extend;
	}

	public void setExtend(Object extend) {
		this.extend = extend;
	}
}
