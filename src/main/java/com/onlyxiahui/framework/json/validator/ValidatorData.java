package com.onlyxiahui.framework.json.validator;

/**
 * Date 2018-12-26 10:16:46<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class ValidatorData<D, A> {

	private String validator;
	private D data;
	private A attribute;
	private Object judge;
	private String code;
	private String message;
	private Object extend;

	public String getValidator() {
		return validator;
	}

	public void setValidator(String validator) {
		this.validator = validator;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public D getData() {
		return data;
	}

	public void setData(D data) {
		this.data = data;
	}

	public A getAttribute() {
		return attribute;
	}

	public void setAttribute(A attribute) {
		this.attribute = attribute;
	}

	public Object getJudge() {
		return judge;
	}

	public void setJudge(Object judge) {
		this.judge = judge;
	}

	public Object getExtend() {
		return extend;
	}

	public void setExtend(Object extend) {
		this.extend = extend;
	}
}
