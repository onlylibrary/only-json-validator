package com.onlyxiahui.framework.json.validator.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * Date 2019-03-08 16:10:01<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class RegexValidatorUtil {

	/**
	 * Date 2019-03-08 15:48:13<br>
	 * Description 正则验证
	 * 
	 * @param regex :正则表达式字符串
	 * @param text  :要匹配的字符串
	 * @return
	 * @since 1.0.0
	 */
	private static boolean match(String regex, String text) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		return matcher.matches();
	}

	/**
	 * 
	 * Date 2019-03-08 15:25:49<br>
	 * Description 验证邮箱
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean 
	 * @since 1.0.0
	 */
	public static boolean isEmail(String text) {
		String regex = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		return match(regex, text);
	}

	/**
	 * 
	 * Date 2019-03-08 15:26:37<br>
	 * Description 验证IPv4地址
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isIpv4(String text) {
		String num = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";
		String regex = "^" + num + "\\." + num + "\\." + num + "\\." + num + "$";
		return match(regex, text);
	}

	/**
	 * 验证网址Url
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isUrl(String text) {
		String regex = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";
		return match(regex, text);
	}

	/**
	 * 验证电话号码
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isTelephone(String text) {
		String regex = "^(\\d{3,4}-)?\\d{6,8}$";
		return match(regex, text);
	}

	/**
	 * 验证输入手机号码
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isMobilephone(String text) {
		String regex = "^[1]+[3,5]+\\d{9}$";
		return match(regex, text);
	}

	/**
	 * 验证输入密码条件(字符与数据同时出现)
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isPassword(String text) {
		String regex = "[A-Za-z]+[0-9]";
		return match(regex, text);
	}

	/**
	 * 验证输入密码长度 (6-18位)
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isPasswordLength(String text) {
		String regex = "^\\d{6,18}$";
		return match(regex, text);
	}

	/**
	 * 验证输入邮政编号
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isPostalCode(String text) {
		String regex = "^\\d{6}$";
		return match(regex, text);
	}

	/**
	 * 验证输入身份证号
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isIdCard(String text) {
		String regex = "(^\\d{18}$)|(^\\d{15}$)";
		return match(regex, text);
	}

	/**
	 * 验证输入一年的12个月
	 * 
	 * @param text
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isMonth(String text) {
		String regex = "^(0?[[1-9]|1[0-2])$";
		return match(regex, text);
	}

	/**
	 * 验证输入一个月的31天
	 * 
	 * @param text
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isDay(String text) {
		String regex = "^((0?[1-9])|((1|2)[0-9])|30|31)$";
		return match(regex, text);
	}

	/**
	 * 验证日期时间
	 * 
	 * @param text
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isDate(String text) {
		// 严格验证时间格式的(匹配[2002-01-31], [1997-04-30],
		// [2004-01-01])不匹配([2002-01-32], [2003-02-29], [04-01-01])
		// String regex =
		// "^((((19|20)(([02468][048])|([13579][26]))-02-29))|((20[0-9][0-9])|(19[0-9][0-9]))-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((01,3-9])|(1[0-2]))-(29|30)))))$";
		// 没加时间验证的YYYY-MM-DD
		// String regex =
		// "^((((1[6-9]|[2-9]\\d)\\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\\d|3[01]))|(((1[6-9]|[2-9]\\d)\\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\\d|30))|(((1[6-9]|[2-9]\\d)\\d{2})-0?2-(0?[1-9]|1\\d|2[0-8]))|(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$";
		// 加了时间验证的YYYY-MM-DD 00:00:00
		String regex = "^((((1[6-9]|[2-9]\\d)\\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\\d|3[01]))|(((1[6-9]|[2-9]\\d)\\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\\d|30))|(((1[6-9]|[2-9]\\d)\\d{2})-0?2-(0?[1-9]|1\\d|2[0-8]))|(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-)) (20|21|22|23|[0-1]?\\d):[0-5]?\\d:[0-5]?\\d$";
		return match(regex, text);
	}

	/**
	 * 验证大写字母
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isUpperChar(String text) {
		String regex = "^[A-Z]+$";
		return match(regex, text);
	}

	/**
	 * 验证小写字母
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isLowerChar(String text) {
		String regex = "^[a-z]+$";
		return match(regex, text);
	}

	/**
	 * 验证验证输入是否为字母
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isLetter(String text) {
		String regex = "^[A-Za-z]+$";
		return match(regex, text);
	}

	/**
	 * 验证验证输入汉字
	 * 
	 * @param text
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isChinese(String text) {
		String regex = "^[\u4e00-\u9fa5],{0,}$";
		return match(regex, text);
	}

	/**
	 * 验证验证输入字符串
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isLength(String text) {
		String regex = "^.{8,}$";
		return match(regex, text);
	}

	/**
	 * 验证输入两位小数
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isTwoDecimal(String text) {
		String regex = "^[0-9]+(.[0-9]{2})?$";
		return match(regex, text);
	}

	/**
	 * 是否为数字
	 * 
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isNumber(String text) {
		String regex = "^(\\-|\\+)?\\d+(\\.\\d+)?$";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(text).matches();
	}

	/**
	 * 验证非零的正整数
	 * 
	 * @param text :待验证的字符串
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isPositiveInteger(String text) {
		String regex = "^\\+?[1-9][0-9]*$";
		return match(regex, text);
	}

	/**
	 * 
	 * Date 2019-03-08 15:29:02<br>
	 * Description 是否为整数
	 * 
	 * @param text
	 * @return boolean
	 * @since 1.0.0
	 */
	public static boolean isInteger(String text) {
		String regex = "^[-\\+]?[\\d]*$";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(text).matches();
	}

}
