package com.onlyxiahui.framework.json.validator.impl;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.json.validator.Validator;
import com.onlyxiahui.framework.json.validator.ValidatorContext;
import com.onlyxiahui.framework.json.validator.ValidatorData;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.error.ErrorInfoEnum;
import com.onlyxiahui.framework.json.validator.util.RegexValidatorUtil;

/**
 * 
 * Date 2018-12-26 17:23:19<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class JsonEmailValidator implements Validator<JSONObject, Object> {

	@Override
	public String getKey() {
		return "email";
	}

	@Override
	public boolean check(Object judge) {
		return true;
	}

	@Override
	public ValidatorResult valid(ValidatorContext validatorContext, ValidatorData<JSONObject, Object> vd) {
		// JSONObject data = vd.getData();
		Object attribute = vd.getAttribute();
		// Object judge = vd.getJudge();
		String code = vd.getCode();
		String message = vd.getMessage();

		ValidatorResult vr = null;
		boolean mark = true;
		if (null != attribute) {
			String text = attribute.toString();
			if (null != text && !"".equals(text)) {
				mark = RegexValidatorUtil.isEmail(text);
			}
		}
		if (!mark) {
			vr = new ValidatorResult();
			vr.setErrorCode(code == null ? this.getCode() : code);
			vr.setMessage(message == null ? this.getMessage() : message);
			vr.setExtend(vd.getExtend());
		}
		return vr;
	}

	public String getMessage() {
		return "不是正确的邮件格式！";
	}

	public String getCode() {
		return ErrorInfoEnum.FORMAT_ERROR.code();
	}
}
