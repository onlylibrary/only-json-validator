package com.onlyxiahui.framework.json.validator.bean;

import java.util.HashMap;

/**
 * Description <br>
 * Date 2020-06-11 11:00:45<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ValidatorInfo extends HashMap<String, ValidatorProperty> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValidatorProperty buildProperty(String key) {
		ValidatorProperty vp = new ValidatorProperty();
		this.put(key, vp);
		return vp;
	}
}
