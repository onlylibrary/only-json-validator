package com.onlyxiahui.framework.json.demo.extend;

import java.util.List;

import com.onlyxiahui.framework.json.demo.util.TextUtil;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.ValidatorService;

/**
 * 
 * Description <br>
 * Date 2019-11-02 15:24:40<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class Extend1A1 {

	public static void main(String[] args) {

		ValidatorService vs = new ValidatorService();
		String json = TextUtil.getTextByClassPath("/demo/extend/data/demo1_0.json");
		String config = TextUtil.getTextByClassPath("/demo/extend/config/config1.json");
		vs.addValidatorConfig(config);

		long time = System.currentTimeMillis();
		List<ValidatorResult> list = vs.validate(json, "001");
		System.out.println(System.currentTimeMillis() - time);
		if (!list.isEmpty()) {
			for (ValidatorResult vr : list) {
				System.out.println(vr.getErrorCode() + ":" + vr.getPropertyPath() + vr.getMessage());
				System.out.println("-----------------:" + vr.getExtend());
			}
		}
	}
}
