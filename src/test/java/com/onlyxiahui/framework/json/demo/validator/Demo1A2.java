package com.onlyxiahui.framework.json.demo.validator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.ValidatorService;

/**
 * 
 * Description <br>
 * Date 2019-11-02 15:24:40<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class Demo1A2 {

	public static void main(String[] args) {
		// ValidatorService vs = new ValidatorService("classpath*:/validator/*.json");
		ValidatorService vs = new ValidatorService("classpath*:/demo/validator/config/config1.json");
		JsonParser jsonParser = new JsonParser();
		String json = "{}";
		InputStream input = null;
		BufferedReader in = null;
		try {
			input = new FileInputStream(Demo1A2.class.getResource("/demo/validator/data/demo1_2.json").getPath());
			in = new BufferedReader(new InputStreamReader(input));
			JsonElement je = jsonParser.parse(in);
			JsonObject jsonObject = je.getAsJsonObject();
			json = jsonObject.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != input) {
					input.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (null != in) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		long time = System.currentTimeMillis();
		List<ValidatorResult> list = vs.validate(json, "001");
		System.out.println(System.currentTimeMillis() - time);
		if (!list.isEmpty()) {
			for (ValidatorResult vr : list) {
				System.out.println(vr.getErrorCode() + ":<" + vr.getPropertyPath() + ">" + vr.getMessage());
			}
		}
	}
}
