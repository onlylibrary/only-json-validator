package com.onlyxiahui.framework.json.demo.validator;

import java.util.List;

import com.onlyxiahui.framework.json.demo.util.TextUtil;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.ValidatorService;

/**
 * 
 * Description <br>
 * Date 2019-11-02 15:24:40<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class Demo5A4 {

	public static void main(String[] args) {

		ValidatorService vs = new ValidatorService();
		String json = TextUtil.getTextByClassPath("/demo/validator/data/demo5_4.json");
		String config = TextUtil.getTextByClassPath("/demo/validator/config/config5.json");
		vs.addValidatorConfig(config);

		long time = System.currentTimeMillis();
		List<ValidatorResult> list = vs.validate(json, "001");
		System.out.println(System.currentTimeMillis() - time);
		if (!list.isEmpty()) {
			for (ValidatorResult vr : list) {
				System.out.println(vr.getErrorCode() + ":" + vr.getPropertyPath() + vr.getMessage());
			}
		}
	}
}
