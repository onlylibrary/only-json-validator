package com.onlyxiahui.framework.json.validator;

import java.math.BigDecimal;

/**
 * date 2018-12-18 08:59:02<br>
 * description 
 * @author XiaHui<br>
 * @since 
 */

public class JsonMaxValidatorTest {
	public static void main(String[] arg) {
		BigDecimal maxBigDecimal = new BigDecimal("55.366");
		BigDecimal bigDecimal = new BigDecimal("55.396");
		System.out.println(maxBigDecimal.compareTo(bigDecimal));
		System.out.println(maxBigDecimal.toString());
	}
}
