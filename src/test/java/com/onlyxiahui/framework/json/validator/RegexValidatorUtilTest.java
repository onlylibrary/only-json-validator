package com.onlyxiahui.framework.json.validator;

import org.junit.Test;

import com.onlyxiahui.framework.json.validator.util.RegexValidatorUtil;

/**
 * date 2018-12-18 08:56:12<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */

public class RegexValidatorUtilTest {
	@Test
	public void isEmail() {
		int i = 0;
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("oo"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("oo@ww"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("oo@cc.c"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("onlyxiahui@qq.c"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("onlovexiahui@qq.com"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("3.5999"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("3d"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail("3fbx"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isEmail(null));
		
		System.out.println("\n");
		System.out.println("\n");
	}
	
	@Test
	public void isNumber() {
		int i = 1;
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("-3"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("+3"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3."));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3.59"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3.5999"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3d"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3f"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3F"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3i"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber("3fbx"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isNumber(null));
		
		System.out.println("\n");
		System.out.println("\n");
	}

	@Test
	public void isInteger() {
		int i = 0;
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("3"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("-3"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("+3"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("3."));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("3.59"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("3.5999"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("3d"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger("3fbx"));
		System.out.println((i++) + ":" + RegexValidatorUtil.isInteger(null));
	}
}
