package com.onlyxiahui.framework.json.validator;

import com.onlyxiahui.framework.json.validator.util.RegexValidatorUtil;

/**
 * date 2018-12-18 08:58:02<br>
 * description 
 * @author XiaHui<br>
 * @since 
 */

public class JsonIntegerValidatorTest {

	public static void main(String arg[]) {
		System.out.println(RegexValidatorUtil.isInteger("3"));
		System.out.println(RegexValidatorUtil.isInteger("-3"));
		System.out.println(RegexValidatorUtil.isInteger("+3"));
		System.out.println(RegexValidatorUtil.isInteger("3."));
		System.out.println(RegexValidatorUtil.isInteger("3.5"));
		System.out.println(RegexValidatorUtil.isInteger("3d"));
		System.out.println(RegexValidatorUtil.isInteger("3fbx"));
	}
}
