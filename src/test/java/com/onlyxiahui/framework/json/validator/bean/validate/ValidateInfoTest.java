package com.onlyxiahui.framework.json.validator.bean.validate;

import java.util.List;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.framework.json.demo.util.TextUtil;
import com.onlyxiahui.framework.json.validator.ValidatorResult;
import com.onlyxiahui.framework.json.validator.ValidatorService;
import com.onlyxiahui.framework.json.validator.bean.ValidateInfo;

/**
 * Description <br>
 * Date 2020-06-11 11:35:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ValidateInfoTest {

	@Test
	public void test1() {
		ValidateInfo info = new ValidateInfo("001");
		info.buildProperty("name").addRule("notBlank", "姓名不能为空", "10001");
		info.buildProperty("roles").addRule("array").getNodes()
				.buildProperty("").addRule("array").getNodes()
				.buildProperty("name").addRule("notBlank", "姓名不能为空", "10001");

		System.out.println(JSONObject.toJSONString(info));
	}

	@Test
	public void test2() {

		String cj = TextUtil.getTextByClassPath("/demo/validator/info/config1.json");
		System.out.println(cj);
		ValidateInfo info = JSONObject.parseObject(cj, ValidateInfo.class);

		System.out.println(JSONObject.toJSONString(info));

		String json = TextUtil.getTextByClassPath("/demo/validator/data/demo1_2.json");
		ValidatorService vs = new ValidatorService("classpath*:/demo/validator/config/config1.json");

		long time = System.currentTimeMillis();
		List<ValidatorResult> list = vs.validate(json, "001");
		System.out.println(System.currentTimeMillis() - time);
		if (!list.isEmpty()) {
			for (ValidatorResult vr : list) {
				System.out.println(vr.getErrorCode() + ":<" + vr.getPropertyPath() + ">" + vr.getMessage());
			}
		}
		System.out.println("-------------------------------------------------------");
		list = vs.validate(json, info.getValidator());
		System.out.println(System.currentTimeMillis() - time);
		if (!list.isEmpty()) {
			for (ValidatorResult vr : list) {
				System.out.println(vr.getErrorCode() + ":<" + vr.getPropertyPath() + ">" + vr.getMessage());
			}
		}
	}
}
