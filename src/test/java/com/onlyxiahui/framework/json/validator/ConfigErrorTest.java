package com.onlyxiahui.framework.json.validator;

import org.junit.Test;

import com.onlyxiahui.framework.json.validator.util.ValidatorJsonUtil;

/**
 * 
 * Description <br>
 * Date 2019-11-02 15:24:40<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class ConfigErrorTest {

	@Test
	public void loadValidatorConfig1() {
		ValidatorService vs = new ValidatorService("classpath*:/validator/config-error.json");
		System.out.println(ValidatorJsonUtil.toJson(vs.map));
		vs.loadValidatorConfig("classpath*:/config/config2.json");
		System.out.println(ValidatorJsonUtil.toJson(vs.map));
	}
}
